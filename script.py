#!/usr/bin/env python3
"""
Write a python script that takes input.txt as input, parses it,
and produces output.txt as output.
The script should be a general script that works on any file
supplied with the format of input.txt.

The script should be run like this:
cat input.txt | python script.py > output.txt
"""

# from __future__ import


__all__ = []
__version__ = '1.0.0'
__author__ = 'Midhun C Nair <midhunch@gmail.com>'
__maintainer__ = 'Midhun C Nair <midhunch@gmail.com>'

import sys
import re


if __name__ == '__main__':
    # Define state variables
    STARS = [0]  # keeps track of stars and inturn gets the indexing
    DOTS = 0  # keeps track of dots and inturn the spacing
    OUT_LINES = []  # to store the output
    LAST_STATE = None  # keeps track of the last_state so as to apply to cont. lines

    # The regex to get the starting (stars, dots or none).
    REG = r'^(?P<starts>[*.]*)(?P<line>[ -~]+)$'

    try:
        # read from the screen
        for line in iter(sys.stdin.readline, b''):
            line = line.strip()  # remove trailing and leading spaces
            if line:  # continue if line exists
                re_obj = re.search(REG, line)  # find the match
                if re_obj:
                    # get the starts with and line to variables
                    starts = re_obj.group('starts').strip()
                    line = re_obj.group('line').strip()

                    # if starts is there that means it should be star/dot
                    if starts:
                        start_len = len(starts)

                        if starts[0] == '*':
                            # reset the dots
                            DOTS = 0

                            # update the STARS state
                            try:
                                STARS = STARS[:start_len]
                                STARS[start_len-1] += 1
                            except IndexError:
                                STARS.append(1)

                            # now add the line to output with necessary changes
                            OUT_LINES.append(
                                '.'.join(map(str, STARS)) + ' ' + line
                            )

                            # update the last state
                            LAST_STATE = (2 * len(STARS)) - 1

                        elif starts[0] == '.':
                            """
                            The logic to handle +/- is handled by comparing the
                            last dots number and current dots number.
                            we know if a star happens the dot is reset to zero.
                            so if dot is not zero and if current dots is greater than
                            previous dots that means the previous line output has to get '+'
                            instead of '-'. and then 
                            """
                            if DOTS != 0 and start_len>DOTS:
                                try:
                                    OUT_LINES[-1] = OUT_LINES[-1].replace('-', '+')
                                except IndexError:
                                    pass

                            DOTS = start_len
                            OUT_LINES.append(
                                ''.join([' ' for _ in range(DOTS)]) + '- ' + line
                            )

                            # update the last state
                            LAST_STATE = DOTS + 2
                    else:  # This means that current line is continuation.
                        if LAST_STATE:
                            # if last_state is there we need to consider that.
                            try:
                                OUT_LINES[-1] = (
                                    OUT_LINES[-1]
                                    + '\n'
                                    + ''.join([' ' for _ in range(LAST_STATE)]) + line
                                )
                            except IndexError:
                                OUT_LINES.append(
                                    ''.join([' ' for _ in range(LAST_STATE)]) + line
                                )
                        else:  # this means the last_state is None
                            try:
                                OUT_LINES[-1] = (
                                    OUT_LINES[-1]
                                    + '\n'
                                    + line
                                )
                            except IndexError:
                                OUT_LINES.append(line)

                else:
                    print("the missed line = ", line)

        print('\n'.join(OUT_LINES))

    except KeyboardInterrupt:
        sys.stdout.flush()

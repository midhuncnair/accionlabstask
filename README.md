# The AccionLabs interview task
Write a python script that takes input.txt as input, parses it,
and produces output.txt as output.
The script should be a general script that works on any file
supplied with the format of input.txt.

The script should be run like this:
cat input.txt | python script.py > output.txt


# input is added in input.txt